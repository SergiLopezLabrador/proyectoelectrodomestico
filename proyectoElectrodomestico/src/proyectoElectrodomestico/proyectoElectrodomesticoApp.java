package proyectoElectrodomestico;

import dto.proyectoElectrodomesticoDto;


public class proyectoElectrodomesticoApp {

	public static void main(String[] args) {
		
		//Aqu� creo los objetos con diferentes nombres
		proyectoElectrodomesticoDto electrodomestico1 = new proyectoElectrodomesticoDto();
		proyectoElectrodomesticoDto electrodomestico2 = new proyectoElectrodomesticoDto(90,5.7);
		proyectoElectrodomesticoDto electrodomestico3 = new proyectoElectrodomesticoDto(100,"marron",'Z',6.9);


        //Aqu� muestro los nombres usando un System.out.println
        System.out.println(electrodomestico1);
        System.out.println(electrodomestico2);
        System.out.println(electrodomestico3);


	}

}
