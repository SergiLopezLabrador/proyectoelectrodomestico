package dto;

public class proyectoElectrodomesticoDto {
	
	//Aqu� creo las variables tipo private para configurar los constructores
	private double precioBase = 100;
	private String color = "blanco";
	private char consumoEnergetico = 'F';
	private double peso = 5.0;
	

	//Aqu� creo un constructor default
	public proyectoElectrodomesticoDto() {
		super();
		this.precioBase = 0.0;
		this.peso = 0.0;
	}

	//Aqu� creo un constructor con el color y el consumo como default
	public proyectoElectrodomesticoDto(double precioBase, double peso) {
		super();
		this.precioBase = precioBase;
		this.color = "blanco";
		this.consumoEnergetico = 'F';
		this.peso = peso;
	}

	//Aqu� creamos un constructor donde el valor del color y del consumo energetico seran dos m�todos
	public proyectoElectrodomesticoDto(double precioBase, String color, char consumoEnergetico, double peso) {
		super();
		this.precioBase = precioBase;
		this.color = comprobarColor(color);
		this.consumoEnergetico = comprobarConsumo(consumoEnergetico);
		this.peso = peso;
	}
	//Aqu� creo un m�todo donde compruebo la letra de consumo que se pone en el main
	public static char comprobarConsumo(char consumoEnergetico) {
        if (consumoEnergetico == 'A' || consumoEnergetico == 'B' ||  consumoEnergetico == 'C' || consumoEnergetico == 'D' || consumoEnergetico == 'F') {
            return consumoEnergetico;
        }else {
            return 'F';
        }
    }
	//Aqu� creo un m�todo donde compruebo que el color que ponene es v�lido
	public static String comprobarColor(String color) {
        if (color.equalsIgnoreCase("blanco") || color.equalsIgnoreCase("negro") ||  color.equalsIgnoreCase("rojo") || color.equalsIgnoreCase("azul") || color.equalsIgnoreCase("gris")) {
            return color;
        }else {
            return "blanco";
        }
    }

	//Aqu� convierto los constructores a string para que al Main salga bien visualmente
	@Override
	public String toString() {
		return "proyectoElectrodomesticoDto [precioBase=" + precioBase + ", color=" + color + ", consumoEnergetico="
				+ consumoEnergetico + ", peso=" + peso + "]";
	}



	
	
	
}


